using OpenQA.Selenium;

namespace CheckLogoutTime
{
    public class Tests
    {
        private IWebDriver driver;

        private readonly By _loginButton = By.XPath("//input[@tabindex='1']");

        private readonly By _passButton = By.XPath("//input[@tabindex='2']");

        private readonly By _passButtonEnterPass = By.XPath("//input[@tabindex='2']");

        private readonly By _SignInButton = By.XPath("//input[@tabindex='4']");

        private readonly By _CreateNewUser = By.XPath("//a[@href='/Consultant/CreateSellerInfo']");

        private readonly By _GoBackButton = By.XPath("//button[@class='el-button el-button--primary']");
        


        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Navigate().GoToUrl("https://qa2.fc2.izumfin.com/ ");
            driver.Manage().Window.Maximize();
          
        }

        [Test]
        public void Test1()
        {
            var loginButton = driver.FindElement(_loginButton);
            loginButton.SendKeys("consultantbm-factoring");

            var passButton = driver.FindElement(_passButton);
            passButton.Click();

            var passButtonEnterPass = driver.FindElement(_passButtonEnterPass);
            passButtonEnterPass.SendKeys("Test123consultant!");

            var SignInButton = driver.FindElement(_SignInButton);
            SignInButton.Click();

            while (true)
            {
                Thread.Sleep(7000);

                var CreateNewUser = driver.FindElement(_CreateNewUser);
                CreateNewUser.Click();

                Thread.Sleep(4000);

                var GoBackButton = driver.FindElement(_GoBackButton);
                GoBackButton.Click(); 
            }



        }
    }
}